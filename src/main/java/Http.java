import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

class HttpCl {
    public static void main(String[] args) throws URISyntaxException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        List<URI> uriList = Arrays.asList(
                new URI("https://swapi.dev/api/people/1/"),
                new URI("https://swapi.dev/api/people/2/"),
                new URI("https://swapi.dev/api/people/3/"),
                new URI("https://swapi.dev/api/people/4/"),
                new URI("https://swapi.dev/api/people/5/"),
                new URI("https://swapi.dev/api/people/6/"),
                new URI("https://swapi.dev/api/people/7/"),
                new URI("https://swapi.dev/api/people/8/"),
                new URI("https://swapi.dev/api/people/9/"),
                new URI("https://swapi.dev/api/people/10/")
        );
        java.net.http.HttpClient client = java.net.http.HttpClient.newBuilder().executor(executorService).build();
        List<CompletableFuture<String>> futures = uriList.stream()
                .map(uri -> client.sendAsync(
                        HttpRequest.newBuilder(uri).build(),
                        HttpResponse.BodyHandlers.ofString())
                        .thenApply(HttpResponse::body)).collect(Collectors.toList());
        futures.forEach(x -> {
            try {
                System.out.println(x.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });
        executorService.shutdownNow();
    }
}