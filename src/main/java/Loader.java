import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class Loader {
    public static void main(String[] args) {
        Employee employee1 = Employee.builder()
                .name("John")
                .experience(4)
                .salary(140)
                .age(27)
                .build();

        Employee employee2 = Employee.builder()
                .name("John")
                .experience(5)
                .salary(104)
                .age(32)
                .build();
        Employee employee3 = Employee.builder()
                .name("Max")
                .experience(6)
                .salary(154)
                .age(44)
                .build();
        Employee employee4 = Employee.builder()
                .name("Orange")
                .experience(7)
                .salary(74)
                .age(74)
                .build();

        List<Employee> people = Arrays.asList(employee1, employee2, employee3, employee4);
        System.out.println("🐼Список упорядочен по ЗП🐼");
        people.sort(Comparator.comparingInt(Employee::getSalary));
        people.forEach(System.out::println);
        System.out.println("🐼Список упорядочен по стажу🐼");
        people.sort(Comparator.comparingInt(Employee::getExperience));
        people.forEach(System.out::println);
        System.out.println("🐼Список упорядочен по возрасту🐼");
        people.sort(Comparator.comparingInt(Employee::getAge));
        people.forEach(System.out::println);
        //------------
        System.out.println("🐼Максимальная ЗП 🐼");
        Employee maxBySalary = people
                .stream()
                .max(Comparator.comparing(Employee::getSalary))
                .orElseThrow(NoSuchElementException::new);

        System.out.println("🐼🐼" + maxBySalary);


        System.out.println("🐼группировка по имени🐼");
        Map<String, List<Employee>> result = people.stream().collect(groupingBy(Employee::getName));

        List<Employee> allMin = result.entrySet().stream()
                .min(Map.Entry.comparingByKey())
                .map(Map.Entry::getValue)
                .orElse(Collections.emptyList());
        allMin.forEach(System.out::println);
        //-такой вариант выводит весь список, но одинаковые имена группирует в строчку(для использованияя // List<> и далее)
        //result.forEach((a,b)-> System.out.println(a + " " + b));

        System.out.println("🐼все с зп более 100🐼");
        List<Employee> employeesWithSalary = people.stream()
                .filter(e -> e.getSalary() >= 100)
                .collect(Collectors.toList());
        employeesWithSalary.forEach(System.out::println);
        //-----


        System.out.println("🐼общая ЗП всех сотрудников🐼");
        int sumBySalary = people
                .stream()
                .mapToInt(Employee::getSalary)
                .sum();
        System.out.println("🐼🐼"+ sumBySalary);


        System.out.println("🐼Средняя ЗП всех сотрудников🐼");
        double avgBySalary = people
                .stream()
                .mapToDouble(Employee::getSalary)
                .average()
                .orElseThrow(NoSuchElementException::new);
        System.out.println("🐼🐼"+ avgBySalary);


    }


}
