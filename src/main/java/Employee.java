import lombok.*;


@Builder
@Data
public class Employee implements Comparable<Employee> {
    private String name;
    private int salary;
    private int experience;
    private int age;




    @Override
    public int compareTo (Employee o1) {
        return this.name.compareTo(o1.name);
    }
}
